import os
from .parse import parser
from .parse import parse_pattern, match_pattern, semantic_search
from .similarity import hyperedge_similarity
from .hypergraph import hedge2token
from .knowledge import knowledge

DICTIONARY_PATH = "/Users/danielvasic/Doktorat/ke/data/relation_dictionary.txt"

file = open (DICTIONARY_PATH, "r")

TRANSITIVE = "TRANZITIVNE RELACIJE"
INVERSE = "INVERZNE RELACIJE"
PATTERNS = "UZORCI ZA PRETRAGU"

transitive = False
inverse = False
patterns = False

TRANISITIVE_RELATIONS = {}
INVERSE_RELATIONS = {}
PATTERN_RELATIONS = {}

for line in file.readlines():
    if TRANSITIVE in line:
        transitive = True
    if INVERSE in line:
        inverse = True
        transitive = False
    if PATTERNS in line:
        patterns = True
        inverse = False
    
    if transitive and "=" not in line:
        relation, erelation = line.rstrip().split("    ")
        TRANISITIVE_RELATIONS[relation] = erelation
    
    if inverse and "=" not in line:
        relation, inverse = line[:-2].split("(")
        INVERSE_RELATIONS[relation] = inverse

    if patterns and "=" not in line:
        relation, pattern = line.rstrip().split("    ")
        PATTERN_RELATIONS[relation] = pattern

def infer(text, knowledge):
    global parser

    parse = parser.parse(text)
    _edge = parse[0]["main_edge"]
    relation = [(transitive_relation, transitive_pattern) for transitive_relation, transitive_pattern in TRANISITIVE_RELATIONS.items() if transitive_pattern in str(_edge)][0][0]
    inverse = INVERSE_RELATIONS[relation]

    result1 = []
    result2 = []


    pattern1 = PATTERN_RELATIONS[relation]
    pattern2 = PATTERN_RELATIONS[inverse]

    parse_pattern(pattern1)
    parse_pattern(pattern2)

    statement = match_pattern(pattern1, _edge, parser)
    for item in knowledge:
        result = match_pattern(pattern1, item, parser)
        if result:
            result1.append(result)
        result = match_pattern(pattern2, item, parser)
        if result:
            result2.append(result)
    
    for item1 in result1:
        for item2 in result2:
            similarity = hyperedge_similarity(
                hedge2token(item1["k2"], parser), 
                hedge2token(item2["e2"], parser)
            )

            qsimilarity = hyperedge_similarity(
                hedge2token(statement["k2"], parser), 
                hedge2token(item2["e1"], parser)
            )

            print(similarity, qsimilarity)

            if similarity >= 0.7 and qsimilarity >= 0.7:
                return True
    return False

print(infer("Računalo se sastoji od aritmetičko-logičke jedinice.", knowledge))