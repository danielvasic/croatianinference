import spacy
from spacy.language import Language
from scipy.spatial.distance import cosine
import numpy as np

class ContextualVectors(object):
    def __init__(self, nlp):
        self._nlp = nlp
        self.fasttext = True
        self.vocab = spacy.load("/Users/danielvasic/Doktorat/nlp/data/hr_fasttext").vocab

    def __call__(self, doc):
        if type(doc) == str:
            doc = self._nlp(doc)
        doc.user_token_hooks["vector"] = self.vector
        doc.user_span_hooks["vector"] = self.vector
        doc.user_hooks["vector"] = self.vector
        doc.user_token_hooks["has_vector"] = self.has_vector
        doc.user_token_hooks["similarity"] = self.similarity
        doc.user_span_hooks["similarity"] = self.similarity
        doc.user_hooks["similarity"] = self.similarity
        return doc
    
    def calculate_average_vector(self, item, method=np.mean):
        doc = item.doc
        return self.vocab.get_vector(item.text.lower()), doc._.trf_data.tensors[0][0][0]
    
    def vector(self, item):
        method = np.average
        if type(item) == spacy.tokens.Token:
            return self.calculate_average_vector(item)
        else:
            vs1 = []
            ts1 = []
            for token in item:
                (v1, t1) = self.calculate_average_vector(token, method=method)
                vs1.append(v1)
                ts1.append(t1)
            return (method(vs1, axis=0), method(ts1, axis=0))

    def has_vector(self, token):
        return True
    
    def similarity(self, obj1, obj2):
        (v1, t1), (v2, t2) = obj1.vector, obj2.vector
        try:
            return ((1 - cosine(v1, v2)) + (1 - cosine(t1, t2))) / 2
        except:
            return 0.0

@Language.factory("trf_vector_hook", assigns=["doc.user_token_hooks", "doc.user_span_hooks", "doc.user_hooks"])
def create_contextual_hook(nlp, name):
    return ContextualVectors(nlp)

def hyperedge_similarity(hedge1, hedge2):
    maxes = []
    hedge1 = sorted(list(hedge1), key=lambda token:token.i)
    hedge2 = sorted(list(hedge2), key=lambda token:token.i)
    for token1 in hedge1:
        sims = []
        for token2 in hedge2:
            if token1.similarity(token2):
                sims.append(token1.similarity(token2))
        if len(sims) > 0:
            maxes.append(max(sims))
    if len(maxes) == 0:
        return 0
    return sum(maxes) / len(maxes)