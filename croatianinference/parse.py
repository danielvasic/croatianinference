import os

from graphbrain import *
from graphbrain.parsers import *
from graphbrain import hyperedge as he

from .hypergraph import *
from .similarity import *


global parser 
parser = ParserHR()
parser.nlp.add_pipe("trf_vector_hook", last=True)
        
def parse_pattern(pattern):
    global parser
    pedge = hedge(pattern)
    words = []
    for atom in pedge.atoms():
        if not atom.is_pattern():
            words.append(atom2str(atom))
    doc = parser.nlp(" ".join(words))
    for atom in pedge.atoms():
        if not atom.is_pattern():
            for token in doc:
                if atom2str(atom) == token.text:
                    parser.atom2token[atom] = token

def match_pattern(pattern, edge, parser):
    pedge = hedge(pattern)
    replacements = []
    for subedge in edge.subedges():
        for subpedge in pedge.subedges():
            if "p" not in subedge.type():
                continue
            if (subedge.is_atom() or subpedge.is_atom()):
                continue
            _subedge = list(hedge2token(subedge, parser))
            _subpedge = list(hedge2token(subpedge, parser))
            if _subedge == None or _subpedge == None:
                continue
            if hyperedge_similarity(_subedge, _subpedge) > 0.80:
                replacements.append((subpedge, subedge, hyperedge_similarity(_subedge, _subpedge)))

    for a1,a2, score in replacements:
        if a1.to_str() != pattern and a2.to_str() != edge.to_str():
            pattern = pattern.replace(a1.to_str(), a2.to_str())
          
    replacements = []       
    for atom1 in edge.atoms():
        for atom2 in pedge.atoms():
            token1 = atom2token(atom1, parser)
            token2 = atom2token(atom2, parser)
            if token1 and token2:
                if token1.similarity(token2) > 0.80:
                    replacements.append((atom1, atom2, token1.similarity(token2)))
    replacements=sorted(replacements, key=lambda x: -x[-1])
    
    for a1,a2, score in replacements:
        if a1.to_str() != pattern and a2.to_str() != edge.to_str():
            pattern = pattern.replace(a2.to_str(), a1.to_str())
    return he.match_pattern(edge, pattern)

def semantic_search(pattern, parses):
    def span_adjustment(span, predicate):
        def remove_i_element_from_span(span, index):
            global parser
            nlp_list = list(span)
            del nlp_list[index]
            if len(nlp_list) == 0:
                return span
            return parser.nlp(" ".join([e.text for e in nlp_list]))
        for idx, token in enumerate(span):
            if token.text == predicate.text:
                span = remove_i_element_from_span(span, idx)
        
        if len(span) == 1:
            if span[0].dep_.lower() in ["appos", "amod", "case"]:
                start_idx = span[0].head.i
                end_idx = span[0].head.i
                if len(list(span[0].head.lefts)) > 0:
                    start_idx = list(span[0].head.lefts)[0].i
                if len(list(span[0].head.rights)) > 0:
                    end_idx = list(span[0].head.rights)[-1].i
                return span[0].doc[start_idx:end_idx+1]
        return span
                
    TAGS = {
        ":Pred",
        ":Acmp",
        ":Agt",
        ":Act",
        ":Aim",
        ":Cause", 
        ":Cond",
        ":Contr",
        ":Dur",
        ":Event", 
        ":Freq",
        ":Goal",
        ":Loc",
        ":Mann",
        ":Means",
        ":Modal",
        ":Mwpred",
        ":Orig",
        ":Pat",
        ":Phras", 
        ":Quant",
        ":Rec",
        ":Reg",
        ":Reslt",
        ":Restr",
        ":Source",
        ":Time"
    }
    
    requested_arguments = []
    results = []
    for item in pattern.split(" "):
        text, tag = item.split(":")
        if ":"+tag not in TAGS:
            raise Exception("The pattern tag {} is not recognized by the system.".format(tag))
        requested_arguments.append((text, tag))
    for parse in parses:
        predicates = []
        for text, tag in requested_arguments:
            if tag == "Pred":
                for token in parse[0]["spacy_sentence"]:
                    if token._.predicate != "_":
                        if text == "*":
                            predicates.append(token)
                        else:
                            if text.lower() == token.text.lower():
                                predicates.append(token)
        
        arguments = {}
        for predicate in predicates:
            for text, tag in requested_arguments:
                if tag != "Pred":
                    for argument, span in predicate._.arguments.items():
                        if argument.upper() == tag.upper():
                            if text == "*":
                                arguments[tag] = span_adjustment(span, predicate)
                                arguments["Pred"] = predicate
                                arguments.update({a[0].upper()+a[1:].lower():span_adjustment(s, predicate) for a, s in predicate._.arguments.items() if a.upper not in ["PRED", tag.upper()]})
                            else:
                                if text.lower() == span.text.lower():
                                    arguments[tag] = span_adjustment(span, predicate)
                                    arguments["Pred"] = predicate
                                    arguments.update({a[0].upper()+a[1:].lower():span_adjustment(s, predicate) for a, s in predicate._.arguments.items() if a.upper not in ["PRED", tag.upper()]})
        results.append(arguments)
    return results


