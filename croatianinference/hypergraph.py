from .parse import parser

def atom2str(atom):
    return atom.to_str().split("/")[0]

def atom2token (atom, parser):
    for atom1, token in parser.atom2token.items():
        if atom2str(atom) == atom2str(atom1):
            return token

def hedge2token(hedge, parser):
    for sedge in  hedge.atoms():
        token = atom2token(sedge, parser)
        if token != None:
            yield token