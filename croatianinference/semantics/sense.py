from nltk import word_tokenize
from .resources.crown import crown
from .config import DATA_PATH
import os

stopwords = open(os.path.join(DATA_PATH, "stopwords-hr.txt")).readlines()
stopwords = set([word.replace("\n", "") for word in stopwords])


def context_intersection( concept, sentence ):
    dictionary = set(word_tokenize(concept.definition))
    
    for i in concept.examples:
         dictionary.union(i)
    dictionary = dictionary.difference( stopwords )
    if isinstance(sentence, str):
        sentence = set(word_tokenize(sentence))

    sentence = set([word.lower() for word in sentence])
    dictionary = set([word.lower() for word in dictionary])
    sentence = sentence.difference(stopwords)
    return len(dictionary.intersection(sentence))

def lesk( word, sentence ):
    supconcept = None
    maxintersection = 0
    for concept in crown.concepts(word):
        intersection = context_intersection(concept,sentence)
        for h in concept.hyponyms:
            intersection += context_intersection( h, sentence )
        if intersection > maxintersection:
                maxintersection = intersection
                supconcept = concept
    if supconcept == None:
        supconcept = crown.concepts(word)[0]
    return supconcept


if __name__ == "__main__":
    word = "stanovništvo"
    sentence = "Stanovništvo određenog područja."
    concept = lesk(word, sentence)
    print(concept)