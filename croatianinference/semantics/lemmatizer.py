import os
from nltk import word_tokenize, sent_tokenize
from .config import DATA_PATH
import sqlite3

class lemmatizer (object):
    db = None
    LEMMA_PATH = os.path.join(DATA_PATH, 'hml', 'hml.db')

    @staticmethod
    def lemmatize(word):
        if lemmatizer.db == None:
            lemmatizer.db = sqlite3.connect(lemmatizer.DATA_PATH)
        query = lemmatizer.db.cursor()
        query.execute("SELECT * FROM words WHERE token='" + word + "'")
        rows = query.fetchall()
        if len(rows) > 0:
            row = rows[0]
        else:
            redak = (word, word, 'Z')
        return (word, redak[0])

    @staticmethod
    def process(text):
        if lemmatizer.db == None:
            lemmatizer.db = sqlite3.connect(lemmatizer.LEMMA_PATH)
            text_sents = sent_tokenize(text)
            sents = []
            for idx, sent in enumerate(text_sents, 1):
                words_text = word_tokenize(sent)
                for word_idx, word in enumerate(words_text, 1):
                    query = lemmatizer.db.cursor()
                    query.execute("SELECT * FROM words WHERE token='" + word + "'")
                    rows = query.fetchall()
                    if len(rows) == 0:
                        msd = "Z"
                    else:
                        row = rows[0]
                        lemma = row[0]
                        msd = row[2]
                    sents.append((word, lemma))
            return sents



if __name__ == "__main__":
    tekst = "Ovo je neki tekst. Potrebno ga je malo obraditi."
    recenice = lemmatizer.process(tekst)
