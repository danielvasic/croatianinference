import json
import os
from ..config import DATA_PATH
from ..lemmatizer import lemmatizer
from collections import defaultdict
from nltk.corpus import wordnet

class Concept(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
    
    @property
    def id (self):
        return self._id

    @property
    def word(self):
        return self._synset[0]['word']

    @property
    def sense(self):
        return self._synset[0]['sense']
    
    @property
    def pos(self):
        return self._pos
    
    @property
    def name(self):
        return "{}.{}.{:02d}".format(
            self.word,
            self.pos,
            self.sense
        )
    
    @property
    def definition(self):
        return self._definition

    @property
    def examples(self):
        return self._examples
    
    @property
    def _synsets(self):
        global crown
        synsets = []
        if len(self._synsets) > 1:
            for synset in self._synsets[1:]:
                naziv = "{}.{}.{:02d}".format(
                    synset['word'],
                    self.pos,
                    synset['sense']
                )
                synsets.append(naziv)
        return synsets
    @property
    def hypernym(self):
        global crown
        if self._hypernym != "":
            return crown._concepts[self._hypernym[0]]

    @property
    def hyponym(self):
        global crown
        _, offset, pos = self._id.split("-")
        synset = wordnet.synset_from_pos_and_offset(pos, int(offset))
        hyponyms = []
        for hyponym in synset.hyponyms():
            offset = str(hyponym.offset()).zfill(8) + '-' + hyponym.pos()
            id = "ENG30-" + offset
            if id in crown._koncepti:
                hyponym.append(crown._koncepti[id])
        return hyponym
            

    def __str__(self):
        return "Concept({})".format(self.naziv)

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def similarity(concept_1, concept_2):
        _, offset, pos = concept_1._id.split("-")
        synset1 = wordnet.synset_from_pos_and_offset(pos, int(offset))

        _, offset, pos = concept_2._id.split("-")
        synset2 = wordnet.synset_from_pos_and_offset(pos, int(offset))

        return synset1.wup_similarity(synset2)

class Crown (object):
    def __init__(self):
        file = os.path.join(DATA_PATH, "crown", "cro_wn30_2012-12-13.json")
        with open(file) as json_file:
            self._data = json.load(json_file)
            self._concepts = {}
            for id, crown_description in self._data.items():
                if len(crown_description['synonyms']) == 0:
                    continue
                examples = crown_description['notes']['usage'] if 'notes' in crown_description and 'usage' in crown_description['notes'] else []
                hypernym = crown_description['ilrs']['hypernym'] if 'hypernym' in crown_description['ilrs'] else ""
                self._concepts[id] = Concept(
                    _id=id, _definition=crown_description['def'], _domena=crown_description['domain'],
                    _examples=examples, _pos=crown_description['pos'],
                    _synsets=crown_description['synonyms'], _hypernym=hypernym
                )
            self._concepts_name = {}
            self._concepts_word = defaultdict(list)
            for id, concept in self._concepts.items():
                self._concepts_name[concept.name] = concept
                self._concepts_word[concept.word].append(concept)

    @property
    def data(self):
        return self._data
    
    @property
    def _concepts_name(self):
        return self._concepts_name
    
    def concepts(self, word=""):
        word = lemmatizer.lematize(word.lower())
        concepts =  self._concepts_name[word[1]]
        
        if len(concepts) > 0:
            return concepts
        else:
            concepts = list()
            for idx, concept in self._concepts.items():
                for synset in concept.synsets:
                    if word.pojavnica in synset:
                        concepts.append(concept)
            return concepts
        return

    
crown = Crown()

if __name__ == "__main__":
    word = "računalo"
    computer = crown.concepts(computer)[0]

    word = "mašina"
    machine = crown.concepts(word)[0]
    
    print(Concept.similarity(computer, machine))
